package com.example.hw_16;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.annotation.DirtiesContext;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class Hw16ApplicationTests {

    @LocalServerPort
    public Integer port;
    RequestSpecification request;

    @BeforeEach
    public void createRestRequest() {
        request = RestAssured.given();
        RestAssured.baseURI = "http://localhost:" + port;
    }

    @Test
    void justTest() {
        Response response = request.get("/multi/4");
        Assertions.assertEquals(200, response.statusCode());
    }


    @ParameterizedTest
    @ValueSource(ints = {2, 3, 4, 5})
    void myFirstApiTesting(int num) {
        HashMap<String, Integer> response = request
                .baseUri(RestAssured.baseURI)
                .log().all()
                .when()
                .get("/multi/" + num)
                .then().log().all()
                .statusCode(HttpStatus.OK.value())
                .extract()
                .body()
                .as(new TypeRef<HashMap<String, Integer>>() {
                });
        int i = 1;
        for (String key : response.keySet()) {
            Assertions.assertEquals(response.get(key), (int) Math.pow(num, i));
            i++;
        }
    }
}
