package com.example.hw_16.logic;

import com.google.gson.Gson;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

public class firstApiLogic {
    public static String multiLogic(int id) {
        HashMap<String, Integer> hashMap = new HashMap<>();
        for (int i = 1; i < 5; i++) {
            hashMap.put(String.valueOf(i), (int) Math.pow(id, i));
        }
        Gson gson = new Gson();
        return gson.toJson(hashMap);
    }
}
